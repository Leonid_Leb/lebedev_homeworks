import java.util.Scanner;//Импортируем сканер
public class homework05 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число:");
        int digit;  //Присваиваем переменную для вводимого числа
        int curDigit; //Присваиваем переменную для последней цифре числа
        int minDigit = 9;  //Присваиваем переменную для минимальной цифры и пока приравниваем к 9(максимум)
        while ((digit = in.nextInt())!= -1) { // Присвиваем введеное число в переменную и сравниваем с -1
            while (digit > 0) {// Цикл будет работать пока число больше 10
                curDigit = digit % 10; // Находим последнюю цифру числа
                if (curDigit < minDigit) // Сравниваем эту цифру с минимальной цифрой и если он меньше
                    minDigit = curDigit;  // Присваиваем минимальной цифре новое значение
                digit /= 10; // Отбрасываем последнюю цифру вводимого числа
            }
            System.out.println("Пока минимальная цифра: " + minDigit); //Выводим минимальную цифру
        }
        System.out.println("Минимальная цифра: " + minDigit); //Выводим минимальную цифру при окончании работы
    }
}



